<?php

	require_once './../partials/template.php';
	
	function get_file_content(){

		?>

			<div class="container-fluid" id="home">

				<!-- start of header -->
				<div class="row py-3">
					<div class="col-sm-12 col-md-2 offset-md-2 mx-auto pb-2">
						<a href="http://www.coral.co.uk">
							<img src="./../assets/images/logo.png" alt="logo">
						</a>
					</div>
					<div class="col-sm-12 col-md-3 offset-md-2 mx-auto pb-2">
						<!-- start of login -->
						<form action="./../controllers/login.php" method="get">
							<div class="input-group text-white" id="login">
							  <input 
							  	type="text"  
							  	placeholder="I'm Already a Customer"
							  	readonly 
							  >
							  <div class="input-group-append">
							    <button class="btn" id="loginBtn">
							    	LOGIN
							    </button>
							  </div>
							</div>
						</form>
						<!-- end of login -->
					</div>
				</div>
				<!-- end of header -->

				<!-- start of main message -->
				<div class="row">
					<div class="col-sm-12 col-md-4 mx-auto" >
						<img src="./../assets/images/main.png" alt="main message" id="main">

						<!-- start of register -->
						<form action="./../controllers/register.php" method="post" id="registerForm">
							<div class="form-group">
								<input 
									type="email" 
									name="email" 
									id="email" 
									placeholder="Email" 
									class="d-block w-50 px-2"
								>
								<!-- start of alert message -->
								<?php 
									if(isset($_SESSION['errors']['email'])){
										?>
											<small class="text-danger" id="errorMessage">
												<?php echo $_SESSION['errors']['email']; ?>
											</small>

										<?php
									}
								?>
								<!-- end of alert message -->
								<button class="btn my-2 w-50 d-block" id="betBtn">BET NOW</button>
							</div>
						</form>
						<!-- end of register -->
					</div>
				</div>
				<!-- end of main message -->

				<!-- start of steps -->
				<div class="row mx-5 text-center" id="steps">
					<div class="col-sm-12 col-md-4 mx-auto">
						<img src="./../assets/images/step1.png" alt="step 1" class="stepImg">
						<img src="./../assets/images/text1.png" alt="" class="stepText">
						<!-- <p class="text-white d-inline">REGISTER & DEPOSIT &#163;/&euro;5 OR MORE</p> -->
					</div>
					<div class="col-sm-12 col-md-4 mx-auto">
						<img src="./../assets/images/step2.png" alt="step 2" class="stepImg">
						<img src="./../assets/images/text2.png" alt="" class="stepText">
					</div>
					<div class="col-sm-12 col-md-4 mx-auto">
						<img src="./../assets/images/step3.png" alt="step 3" class="stepImg">
						<img src="./../assets/images/text3.png" alt="" class="stepText">
					</div>
				</div>
				<!-- end of steps -->

				<!-- start of banner message -->
				<div class="row" >
					<div class="col-sm-12 col-md-3 mx-auto">
						<img src="./../assets/images/banner.png" alt=""  id="banner">
					</div>
				</div>
				<!-- end of banner message -->

				<!-- start of coral info -->
				<div class="row">
					<div class="col-sm-12 col-md-4 mx-auto" id="coralInfo">
						<p class="text-white">
							<button type="button" class="text-white" id="aboutBtn" onclick="slide()">
							  <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-caret-right-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path d="M12.14 8.753l-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z"/>
							</svg>
							About Coral Sports Betting
							</button>|
							<!-- <a href="#" class="text-white">Terms & Conditions</a> -->
							<button type="button" class="text-white"data-toggle="modal" data-target="#terms">
							  Terms & Conditions
							</button>
						</p>
						<div class="modal fade" id="terms" tabindex="-1">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title">Terms & Conditions</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body mx-2">
						        <ol>
						        	<li>The £30 free bet offer is available for new coral.co.uk customers who deposit and bet a total of £30 on any sports event(s).</li>
						        	<li>The free bet will be triggered by the first deposit amount and will not be applicable for subsequent deposit values.</li>
						        	<li>Customers depositing and staking less than £30 will receive the equivalent free bet value. For example: a customer that deposits an initial amount of £5 will be credited with a £5 free bet once they have bet a total of £5 on any sports event(s).</li>
						        	<li>Advertised offers relating to the opening of a Coral.co.uk account are limited to one per customer.</li>
						        	<li>In the case of a customer opening more than one Coral.co.uk account, we reserve the right to suspend and close duplicate accounts, and make void any bets placed.</li>
						        	<li>Please note that the free bet will only appear in your account once the amount of your own deposit has been bet with on any sporting event(s) (this doesn't have to be bet as one go but can be bet in increments). The free bet must then be staked before any withdrawal is permitted. Void bets do not count toward this offer.</li>
						        	<li>The free bet can be used on "sports" events and are not valid for Games, Casino, Virtual Sports, Poker, Lotto, Bingo, Live Casino.</li>
						        	<li>The free bet can be redeemed on win or each-way bets and can used on the following listed bet types: single, double, treble, 4-fold and upwards accumulator, forecast, combination forecast, tricast and combination tricast.</li>
						        	<li>The original free bet stake is not returned with any winnings.</li>
						        	<li>The free bet is not valid on tote, other combination and multiple bets (i.e Lucky 15s)</li>
						        	<li>The free bet is valid for 30 days.</li>
						        	<li>We reserve the right to amend or withdraw any promotion at any point for whatever reason.</li>
						        	<li>Coral.co.uk rules apply.</li>
						        	<li>Offer only available to UK Residents aged 18 or over.</li>
						        </ol>

						      </div>
						    </div>
						  </div>
						</div>
					</div>
				</div>
				<!-- end of coral info -->
		<?php
		unset($_SESSION['errors']);

	}
